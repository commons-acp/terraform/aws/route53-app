# ACM Certificate generation
resource "aws_acm_certificate" "cert_app" {
  provider          = aws-cloudfront
  domain_name       = var.front_subdomain
  validation_method = "DNS"
}
resource "aws_acm_certificate_validation" "cert_app" {
  provider                = aws-cloudfront
  certificate_arn         = aws_acm_certificate.cert_app.arn
  validation_record_fqdns = [ for record in aws_route53_record.cert_validation_app : record.fqdn]
}

resource "aws_route53_record" "cert_validation_app" {
  for_each = {
  for dvo in aws_acm_certificate.cert_app.domain_validation_options : dvo.domain_name => {
    name   = dvo.resource_record_name
    record = dvo.resource_record_value
    type   = dvo.resource_record_type
  }
  }

  allow_overwrite = true
  name            = each.value.name
  records         = [each.value.record]
  ttl             = 60
  type            = each.value.type
  zone_id         = var.zone_id
}
